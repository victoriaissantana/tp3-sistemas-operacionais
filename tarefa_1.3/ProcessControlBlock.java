package mars.mips.SO;
import mars.mips.hardware.Register;
import mars.mips.hardware.RegisterFile; 

/**
 * @author iris
 * Classe para armazenar informações do contexto do processo
 * */
public class ProcessControlBlock {

	RegisterFile reg;
	Register[] conteudo = reg.getRegisters();
	int enderecoInicio;
	int PID;
	int estado; // 1 - pronto; 2 - executando;
	
	public Register[] getConteudo() {
		return conteudo;
	}
	public void setConteudo(Register[] conteudo) {
		this.conteudo = conteudo;
	}
	public int getEnderecoInicio() {
		return enderecoInicio;
	}
	public void setEnderecoInicio(int enderecoInicio) {
		this.enderecoInicio = enderecoInicio;
	}
	public int getPID() {
		return PID;
	}
	public void setPID(int pID) {
		PID = pID;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	
	
}
