package mars.mips.instructions.syscalls;

import mars.ProcessingException;
import mars.ProgramStatement;
import mars.mips.hardware.RegisterFile;

public class SyscallProcessChange extends AbstractSyscall {

	public SyscallProcessChange(int number, String name) {
		super(number, name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void simulate(ProgramStatement statement) throws ProcessingException {
		// TODO Auto-generated method stub

	}
	
	int numChamadaSistema = RegisterFile.getValue(2); 
	/*
	 * o número da chamada de sistema
	 * SyscallFork passado no registrador $v0 
	 * */

	public SyscallProcessChange(int number, String name, int numChamadaSistema) {
		super(number, name);
		this.numChamadaSistema = numChamadaSistema;
	}
	
	public void executar() {
		
	}

}
