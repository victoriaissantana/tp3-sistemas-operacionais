package mars.mips.instructions.syscalls;

import mars.ProcessingException;
import mars.ProgramStatement;
import mars.mips.hardware.RegisterFile;
/**
 * @author iris
 * */
public class SyscallFork extends AbstractSyscall {

	public SyscallFork(int number, String name) {
		super(number, name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void simulate(ProgramStatement statement) throws ProcessingException {
		// TODO Auto-generated method stub

	}
	
	int numChamadaSistema = RegisterFile.getValue(2); 
	/*
	 * o número da chamada de sistema
	 * SyscallFork passado no registrador $v0 
	 * */
	int enderecoInicial = RegisterFile.getValue(4);
	/*
	 * endereço inicial do processo
	 * (representado por um label) passado no registrador $a0
	 * */
	public SyscallFork(int number, String name, int numChamadaSistema, int enderecoInicial) {
		super(number, name);
		this.numChamadaSistema = numChamadaSistema;
		this.enderecoInicial = enderecoInicial;
	}
	
	
	

}
